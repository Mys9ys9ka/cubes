<?php

namespace App\Http\Controllers\Ajax;

use App\Battle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BattleStart extends Controller
{
    public function start(Request $request){
//        dd(json_encode($request->all()));
        $battle = new Battle();
        $battle->first_data = json_encode($request->all());
//        dd($battle);
        $battle->save();
        return $battle->id;
    }
    public function battleData(Request $request){

    }
}
