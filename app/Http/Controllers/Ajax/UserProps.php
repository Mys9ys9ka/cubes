<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserProps extends Controller
{
    public function props(Request $request){
        if(Auth::user()){
//          dd(session()->has('User_info'));
//            dd(Auth::user()->all());

            // массив слотов для экипировки
            $arEquipTemplateSlot = array(
                'handLeft',
                'handRight'
            );

            //  выгружаем массив предметов на игроке
            $userId = 1;
            $UserEquipDB = DB::table('User_Equip')->where('id', '=', $userId)
                ->first();
            // экипированные предметы с характеристиками из билиотеки
            $arUserEquip = [];
            foreach ($UserEquipDB as $slot=>$id) {
//                dd($slot);
                if(in_array($slot, $arEquipTemplateSlot)){
                    $arUserEquip[$slot] = $id;
                }
            }

            // выгружаем предметы нахадящиеся в сумке
            $UserBagDB = DB::table('User_Bag')->where('id', '=', $userId)
                ->first();
            $arBag = [];
            foreach (json_decode($UserBagDB->items, true) as $itemID=>$item_status){
                $UserBagDB = DB::table('Biblio_Items')->where('id', '=', $itemID)
                    ->first();
                $arBag[$UserBagDB->id]['info'] = $UserBagDB;
                $arBag[$UserBagDB->id]['status'] = $item_status;
            }

            // массив данных об игроке
            $arUserInfo = array(
                'id' => $userId,
                'name' => Auth::user()->name,
                'avatar' => Auth::user()->avatar,
                'equip' => $arUserEquip,
                'bag' => $arBag
            );
            return $arUserInfo;
        }
    }
}
