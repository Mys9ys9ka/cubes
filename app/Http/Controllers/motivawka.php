<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class motivawka extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function execute(Request $request){
        return view('motivawka');
    }
}
