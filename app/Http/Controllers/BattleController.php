<?php

namespace App\Http\Controllers;

use App\Battle;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BattleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function execute(Request $request){

        $BattlePlayer = Battle::where('id', '=', $request->id)->first();

        $arPlayer = (array)json_decode($BattlePlayer['first_data']);
//        dd($arPlayer);
//        $UserEquipDB = DB::table('User_Equip')->where('User_Equip.id', '=', $arPlayer['userId'])
//            ->join('Biblio_Items', 'Biblio_Items.id', '=', 'User_Equip.id')
//            ->select('User_Equip.handLeft','User_Equip.handRight')
//            ->first();
//        $UserEquip = DB::table('Biblio_Items')
//            ->whereIn('id', (array)$UserEquipDB)
//            ->get();
//
//        $users = DB::table('users')->where('users.id', '=', $arPlayer['userId'])
//            ->join('User_Equip', 'users.id', '=', 'User_Equip.id')
//            ->join('Biblio_Items', 'Biblio_Items.id', '=', 'User_Equip.id')
//            ->join('User_BattleProps', 'User_BattleProps.id', '=', 'users.id')
//            ->select('users.id', 'users.status', 'User_Equip.handLeft', 'User_Equip.handRight', 'User_BattleProps.*')
//            ->first();
//        $UserItems = DB::table('Biblio_Items')
//            ->whereIn('id', array($users->handLeft, $users->handRight))
//            ->get();

        return view('battle');
    }
}
