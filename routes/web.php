<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Artisan::call('view:clear');

Route::get('/', 'WelcomeController@index');

Auth::routes();

// логин через соцсети
Route::post('ulogin', 'Auth\UloginController@login');


Route::get('/home', 'HomeController@index');
Route::get('/battle', 'BattleController@execute')->name('battle');
Route::get('/map', 'MapController@execute')->name('map');
Route::get('/shop', 'ShopController@execute')->name('shop');


Route::post('/battleStart', 'Ajax\BattleStart@start');
Route::post('/battleData', 'Ajax\BattleStart@battleData');

Route::post('/userProps', 'Ajax\UserProps@props');
Route::get('/motivawka', 'motivawka@execute')->name('motivawka');
//Route::post('/battleStart', 'Ajax\BattleStart@battleData');
