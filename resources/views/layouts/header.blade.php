<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Кубы судьбы</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">

    <script src="{{ asset('public/js/jquery-3.0.0.min.js') }}"></script>
    <script src="{{ asset('public/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>

</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="user_nik ">{{ Auth::user()->nik }}</div>
        <div class="equip_btn">Экипировка</div>
    </div>
</nav>
<div class="container">

    <div class="equip_block">
        <div class="avatar_equip_box">
            <div class="header_block"></div>
            <div class="left_item_block">
                <div class="item_box item1 helmet"></div>
                <div class="item_box item2 handLeft"></div>
                <div class="item_box item3 shoes"></div>
            </div>
            <div class="avatar_block"></div>
            <div class="right_item_block">
                <div class="item_box item4 armor"></div>
                <div class="item_box item5 handRight"></div>
                <div class="item_box item6 pants"></div>
            </div>
            <div class="footer_item_block">
                <div class="item_box"></div>
                <div class="item_box item_middle"></div>
                <div class="item_box item_end"></div>
            </div>
        </div>
        <div class="props_box"></div>
        <div class="bag_box">
            <div class="money_box">
                <div class="money_slot"></div>
                <div class="money_slot"></div>
                <div class="money_slot"></div>
            </div>
            <div class="bag_item">1</div>
            <div class="bag_item">1</div>
            <div class="bag_item">1</div>
            <div class="bag_item">1</div>
            <div class="bag_item">1</div>
            <div class="bag_item">1</div>
        </div>
    </div>
</div>
<style>
    .equip_block{
        position: relative;
        top:-20px;
        display: inline-block;
        /*left: 50%;*/
        /*transform: translateX(-50%);*/
        /*display: none;*/
        width: 100%;
        min-height: 35px;
        border: 2px solid grey;
    }
    .avatar_equip_box{
        position: relative;
        display: inline-block;
        float: left;
        margin: 7px 0px 7px 7px ;
        width: 354px;
        height: 454px;
        border: 2px solid grey;
    }
    .props_box{
        position: relative;
        display: inline-block;
        float: left;
        margin: 7px 0px 7px 7px ;
        width: 300px;
        height: 454px;
        border: 2px solid grey;
    }
    .bag_box{
        position: relative;
        display: inline-block;
        float: left;
        margin: 7px;
        width: 454px;
        height: 454px;
        border: 2px solid grey;
    }
    .header_block{
        display: inline-block;
        float: left;
        width: 100%;
        height: 50px;
    }
    .left_item_block{
        display: inline-block;
        float: left;
        width: 100px;
        height: 300px;
        background-color: rgba(40,40,40,0.1);
    }
    .avatar_block{
        display: inline-block;
        float: left;
        width: 150px;
        height: 300px;
        background-color: rgba(40,40,40,0.3);
    }
    .right_item_block{
        display: inline-block;
        float: left;
        width: 100px;
        height: 300px;
        background-color: rgba(40,40,40,0.1);
    }
    .footer_item_block{
        display: inline-block;
        width: 100%;
        height: 100px;
        background-color: rgba(40,40,40,0.2);
    }
    .item_box{
        display: inline-block;
        width: 100px;
        height: 100px;
        border: 1px solid #bce8f1;
        float: left;
    }
    .item_box:hover{
        cursor: pointer;
        box-shadow: 0px 0px 5px 1px #bce8f1;
    }
    .item_end{
        float: right;
    }
    .item_middle{
        width: 150px;
    }
    .money_box{
        display: inline-block;
        width: 100%;
        height: 45px;
    }
    .money_slot{
        display: inline-block;
        width: 30%;
        height: 45px;
        margin: 2% 0px 0px 2%;
        border: 1px solid grey;
    }
    .bag_item{
        display: inline-block;
        float: left;
        width: 85px;
        margin: 4px 0px 0px 4px;
        height: 85px;
        border: 1px solid grey;
    }
</style>
<script>
    $(document).ready(function () {
        window.arUserBattleProps = [];
        $.post(
            '/userProps',
            function (result) {
                console.log('result', result);
                userPropSet(result)
            }
        );

        function userPropSet(arProp) {
            console.log('arProp', arProp);
            setUserEquip(arProp['equip'], arProp['bag']);
        }
        function setUserEquip(arEquip,arBag) {

            console.log('arEquip', arEquip);
            // var equips = JSON.parse(arEquip);
            // console.log('equips', equips);
            // выбираем экипированные вещи и по айди достаем свойства из списка вещей в рюкзаке
            $.each(arEquip, function (slotName,id) {
                var equipItem = arBag[id];
                var props = JSON.parse(equipItem['info']['props']);
                console.log('slotName', arBag[id], 'props', props);
                // var props = JSON.parse(arProp['props']);
                // arUserBattleProps[itemName] = props;
                // console.log('parseJson', JSON.parse(arProp['props']));
                console.log('slotName', slotName);
                $('.avatar_equip').find('.'+slotName).html(
                    '<img src="" alt="'+equipItem['info']['name']+'">'
                );
            });
        }
    });
</script>

<?// dd(session()->all());?>