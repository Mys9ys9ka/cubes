@include('layouts.header')
<div class="container">
    <div class="add_new_task" title="Добавить новую задачу">+</div>
    <i class="fa fa-futbol-o" aria-hidden="true"></i>
    <div class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
    </div>
    <button type="button" class="btn btn-primary">Primary</button>
    <button type="button" class="btn btn-secondary">Secondary</button>
    <button type="button" class="btn btn-success">Success</button>
    <button type="button" class="btn btn-danger">Danger</button>
    <button type="button" class="btn btn-warning">Warning</button>
    <button type="button" class="btn btn-info">Info</button>
    <button type="button" class="btn btn-light">Light</button>
    <button type="button" class="btn btn-dark">Dark</button>

    <button type="button" class="btn btn-link">Link</button>
</div>

<style>
    .equip_block{
        display: none;
    }
    /*.container{*/
        /*!*background: rgb(47, 79, 79);*!*/

    /*}*/
    .add_new_task{
        font-size: 28px;
        color: rgb(47, 79, 79);
        cursor: pointer;
    }
    .task_category_block{
        display: inline-block;
        width: 100%;
    }
    .fa{
        width: 24px;
        display: inline-block;
        cursor: pointer;
        color: rgb(40,40,40)!important;
        font-size: 24px;
    }
</style>
<script>
    $(document).ready(function () {
        $('.add_new_task').on('click', function () {
            $('#newTaskModal').modal('show');
        });
    });
</script>
@include('layouts.footer')
<div id="newTaskModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"
                    style="float:right;padding:5px 10px 0 0;z-index:1;position:relative;">&times;
            </button>
            <div class="modal-header">
                <span class="modal-title">Новая задача</span>
            </div>
            <div class="modal-body">
                <p>Выбрать рубрику</p>
                <div class="task_category_block">
                    <i class="fa fa-futbol-o" aria-hidden="true" title="спорт"></i>
                    <i class="fa fa-glass" aria-hidden="true" title="выпивка"></i>
                    <i class="fa fa-language" aria-hidden="true" title="иностранные языки"></i>
                    <i class="fa fa-vk" aria-hidden="true" title="соцсети"></i>
                    <i class="fa fa-wrench" aria-hidden="true" title="ремонт"></i>
                    <i class="fa fa-money" aria-hidden="true" title="заработок"></i>
                    <i class="fa fa-book" aria-hidden="true" title="чтение"></i>
                    <input type="hidden" class="task_category">
                </div>
            </div>
            <div class="modal-footer">
                <div class="add_task_to_BD">Добавить</div>
            </div>
        </div>
    </div>
</div>