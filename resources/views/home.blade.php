@include('layouts.header')
<i class="fa fa-futbol-o" aria-hidden="true" title="спорт"></i>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                    <div class="user_nik ">{{ Auth::user()->nik }}</div>
                </div>
            </div>
        </div>
    </div>
    <a href="{{ route('battle', ['id' => 1]) }}">
        <span class="mob_name">В бой</span>
    </a>
    <a href="{{ route('map', ['id' => 1]) }}">
        <span class="mob_name">Karta</span>
    </a>
    <a href="{{ route('shop', ['id' => 1]) }}">
        <span class="mob_name">Shop</span>
    </a>
    <?$json = array(
        'type'=> 'attack',
        'cube'=> array(1,1,2,2)
    );
    ?>
    <script>
        $(document).ready(function () {
            var cubes = '<?=json_encode($json)?>';
           console.log('window', JSON.parse(cubes));
        });
    </script>
</div>

@include('layouts.footer')