@include('layouts.header')
<div class="container">
    <div class="title">map</div>
    <div class="map_container">
        <div class="tag mob move_click" data-id="1" data-move_type="battle">mob</div>
        <div class="tag shop move_click" data-id="1" data-move_type="shop">shop</div>
        <div class="tag user" data-id="<?=session()->get('User_info')['id']?>">user</div>
    </div>
<?//dd(session()->get('User_info'))?>
</div>
<style>
    .map_container{
        display: block;
        height: 450px;
        border: 2px solid grey;
        position: relative;
    }
    .tag{
        position: absolute;
        display: inline-block;
        width: 50px;
        height: 50px;
        border: 1px solid rgb(40,40,40);
        border-radius: 50% 50% 50% 0;
        padding: 3px;
        transform: rotate(-45deg);
        box-shadow: 4px -4px 8px 2px grey;
        background-color: rgb(255,255,255);
        /*box-shadow: 0px 1px 57px 3px rgba(0,1,1,0.19);*/
    }
    .shop{
        left: 400px;
        top:70px;
    }
</style>
<script>
    $(document).ready(function () {
        console.log('.map_container', $('.map_container').width(), $('.map_container').height());
        var first_mob_top = randomInteger(40, $('.map_container').height()-40);
        var first_mob_left = randomInteger(25, $('.map_container').width()-25);
        console.log('first_mob_top', first_mob_top, 'first_mob_left', first_mob_left);
       $('.mob').css({'top':first_mob_top+'px','left':first_mob_left+'px'});
       // кнопка начала движения
       $('.map_container').on('click', '.move_click', function () {
           move_start($('.user'), $(this));
           // var mob_position = $mob.offset();
           // var user_position = $user.offset();
           // go_to_enemy($user, mob_position, user_position);

       });
    });
    function move_start($user,$object) {
        var object_position = $object.offset();
        var user_position = $user.offset();
        go_to_object($user, object_position, user_position);
        console.log('$object', $object.data('move_type'));
        setTimeout(function() {
            switch ($object.data('move_type')) {
                case 'shop':
                    console.log('idy v shop');
                    go_in_shop($object.data('id'));
                    break;
                case 'battle':
                    console.log('idy v battle');
                    go_battle_start($user.data('id'),$object.data('id'));
                    break;
                default:
                    console.log('idy kyda to');
            }

        }, 900);
        console.log('$user', $user, '$object', $object);
    }
    function randomInteger(min, max) {
        // случайное число от min до (max+1)
        let rand = min + Math.random() * (max + 1 - min);
        return Math.floor(rand);
    }
    function enter_to_shop() {
        
    }
    function go_battle_start(userId, mobId) {
        $.post(
            '/battleStart',
            {
                mobId:mobId,
                userId: userId
            },
            function (result) {
                console.log('result', result);
                location.href = '/battle?id='+result;
            }
        );
    }
    function go_in_shop(shopId) {
        location.href = '/shop?id='+shopId;
    }
    function go_to_object(user, object_position, user_position) {
        var $left_step = (object_position.left-user_position.left)/10;
        var $top_step = (object_position.top-user_position.top+10)/10;
        console.log('$left_step', $left_step, '$top_step', $top_step);
        console.log('func');
        var i = 0;
        setInterval(function() {
            if(i<10){
                $(user).offset({left:user_position.left+=$left_step, top:user_position.top+=$top_step});
                i++;
                console.log('step')
            } else {
                clearInterval();
                // go_battle();
            }
        }, 100);
    }
</script>
@include('layouts.footer')